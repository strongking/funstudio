﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Image
{
    public class ImageModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}