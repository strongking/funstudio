﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Image
{
    public class ImageProcessModel
    {
        public long? FrameId { get; set; }
        public long? LayoutId { get; set; }
        public long? ThemeId { get; set; }
        public string TransactionId { get; set; }
        public List<string> ListImages { get; set; } = new List<string>();
    }
}