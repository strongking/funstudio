﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Entities
{
    public class FilterEntity
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}