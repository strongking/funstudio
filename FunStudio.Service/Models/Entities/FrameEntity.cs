﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Entities
{
    public class FrameEntity
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public List<ImageEntity> Images { get; set; } = new List<ImageEntity>();
    }

    public class ImageEntity
    {
        public string Image { get; set; }
    }
}