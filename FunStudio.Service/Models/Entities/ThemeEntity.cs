﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Entities
{
    public class ThemeEntity
    {
        public long? Id { get; set; }
        public long? FrameId { get; set; }
        public long? LayoutId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
    }
}