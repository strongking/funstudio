﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Entities
{
    public class ElementEntity
    {
        public long? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public double? X { get; set; }
        public double? Y { get; set; }
        public int? OrderNo { get; set; }
    }
}