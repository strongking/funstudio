﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunStudio.Service.Models.Response
{
    public class ResponseModel
    {
        public int? totalRecord { get; set; } = 0;
        public object data { get; set; }
    }
}