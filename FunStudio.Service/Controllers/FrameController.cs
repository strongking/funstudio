﻿using FS.Common.HttpCustom;
using FunStudio.Service.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FunStudio.Service.Controllers
{
    [RoutePrefix("api/frame")]
    public class FrameController : ApiController
    {
        /// <summary>
        /// Danh sách frame
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlist")]
        public HttpResponseMessage GetList()
        {
            try
            {
                var data = new ResponseModel();
                if (WebApiConfig.Frames != null && WebApiConfig.Frames.Count > 0)
                {
                    data.totalRecord = WebApiConfig.Frames.Count;
                    data.data = WebApiConfig.Frames;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Chi tiết của 1 frame
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyid")]
        public HttpResponseMessage GetById(long? id)
        {
            try
            {
                var data = WebApiConfig.Frames.FirstOrDefault(x => x.Id == id);
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
    }
}
