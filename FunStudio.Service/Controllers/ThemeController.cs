﻿using FS.Common.HttpCustom;
using FunStudio.Service.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FunStudio.Service.Controllers
{
    [RoutePrefix("api/theme")]
    public class ThemeController : ApiController
    {
        /// <summary>
        /// Danh sách theme
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlist")]
        public HttpResponseMessage GetList()
        {
            try
            {
                var data = new ResponseModel();
                if (WebApiConfig.Themes != null && WebApiConfig.Themes.Count > 0)
                {
                    data.totalRecord = WebApiConfig.Themes.Count;
                    data.data = WebApiConfig.Themes;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Chi tiết của 1 theme
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyid")]
        public HttpResponseMessage GetById(long? id)
        {
            try
            {
                var data = WebApiConfig.Themes.FirstOrDefault(x => x.Id == id);
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Danh sách theme theo frame
        /// </summary>
        /// <param name="frameId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyframeid")]
        public HttpResponseMessage GetByFrameId(long? frameId)
        {
            try
            {
                var data = new ResponseModel();
                var dataLayout = WebApiConfig.Themes.Where(x => x.FrameId == frameId).ToList();
                if (dataLayout != null && dataLayout.Count > 0)
                {
                    data.totalRecord = dataLayout.Count;
                    data.data = dataLayout;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Danh sách theme theo layout
        /// </summary>
        /// <param name="layoutId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbylayoutid")]
        public HttpResponseMessage GetByLayoutId(long? layoutId)
        {
            try
            {
                var data = new ResponseModel();
                var dataTheme = WebApiConfig.Themes.Where(x => x.LayoutId == layoutId).ToList();
                if (dataTheme != null && dataTheme.Count > 0)
                {
                    data.totalRecord = dataTheme.Count;
                    data.data = dataTheme;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
    }
}
