﻿using FS.Common.HttpCustom;
using FunStudio.Service.Models.Image;
using FunStudio.Service.Models.Response;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace FunStudio.Service.Controllers
{
    [RoutePrefix("api/file")]
    public class FileController : ApiController
    {
        /// <summary>
        /// Lấy danh sách hình ảnh trong folder transaction
        /// </summary>
        /// <param name="transactionid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("images/{transactionid}")]
        public HttpResponseMessage GetListFileImg(string transactionid)
        {
            try
            {
                string directory = HttpContext.Current.Server.MapPath("~/App_Data/FileMedia/" + transactionid);
                string[] fileNames = Directory.GetFiles(directory);

                // Filter the list to include only image files
                fileNames = fileNames.Where(f =>
                    f.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) ||
                    f.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase) ||
                    f.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ||
                    f.EndsWith(".gif", StringComparison.OrdinalIgnoreCase)
                ).ToArray();
                fileNames = fileNames.Select(f => Path.GetFileName(f)).ToArray();
                var data = new ResponseModel();
                if (fileNames != null && fileNames.Count() > 0)
                {
                    data.totalRecord = fileNames.Count();
                    data.data = fileNames;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
        /// <summary>
        /// Lấy 1 hình ảnh trong folder transaction
        /// </summary>
        /// <param name="transactionid"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("image/{transactionid}/{filename}")]
        public HttpResponseMessage GetFileImg(string transactionid, string filename)
        {
            try
            {
                string imagePath = HttpContext.Current.Server.MapPath("~/App_Data/FileMedia/" + transactionid + "/" + filename);
                if (!File.Exists(imagePath))
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "The requested image was not found");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
                string fileExtension = Path.GetExtension(filename).ToLower();
                string contentType;
                switch (fileExtension)
                {
                    case ".jpg":
                    case ".jpeg":
                        contentType = "image/jpeg";
                        break;
                    case ".png":
                        contentType = "image/png";
                        break;
                    case ".gif":
                        contentType = "image/gif";
                        break;
                    default:
                        contentType = "application/octet-stream";
                        break;
                }
                byte[] imageData = File.ReadAllBytes(imagePath);

                // Create a response with the image data and content type
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(imageData);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                return response;
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Xử lý hình ảnh cuối cùng 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("processimage")]
        public HttpResponseMessage ProcessImage(ImageProcessModel model)
        {
            try
            {
                // Lấy thông số cấu hình của frame
                var frame = WebApiConfig.Frames.FirstOrDefault(x => x.Id == model.FrameId);
                // Lấy thông số cấu hình của layout
                var layout = WebApiConfig.Layouts.FirstOrDefault(x => x.Id == model.LayoutId);
                // Lấy thông số cấu hình của theme
                var theme = WebApiConfig.Themes.FirstOrDefault(x => x.Id == model.ThemeId);
                string directory = HttpContext.Current.Server.MapPath("~/App_Data/");
                var pathCheck = Path.Combine(directory, "FileMedia", model.TransactionId);
                if (model.ListImages.Count() > 0)
                {
                    if (Directory.Exists(pathCheck))
                    {
                        // Lưu hình ảnh đã chọn sang dạng image
                        var listImage = new List<Image>();
                        foreach(var item in model.ListImages)
                        {
                            var image = Image.FromFile($@"{pathCheck}\{item}");
                            listImage.Add(image);
                        }
                        // Tải hình ảnh mẫu của layout
                        var imageLayout = Image.FromFile(HttpContext.Current.Server.MapPath(layout.Image));
                        // Tạo 1 Graphics từ hình ảnh layout
                        Graphics graphics = Graphics.FromImage(imageLayout);
                        if(layout.Pictures.Count()> 0)
                        {
                            int i = 0;
                            foreach(var item in layout.Pictures)
                            {
                                // Gắn các hình ảnh vào khung
                                graphics.DrawImage(listImage[i], new Rectangle((int)item.X, (int)item.Y, (int)item.Width, (int)item.Height));
                                ++i;
                            }
                        }
                        // Layer theme tạo ở trên cùng
                        var imageTheme = Image.FromFile(HttpContext.Current.Server.MapPath(theme.Image));
                        graphics.DrawImage(imageTheme, new Rectangle(0, 0, (int)theme.Width, (int)theme.Height));
                        imageLayout.Save(@"E:\fl.png");
                        MemoryStream ms = new MemoryStream();
                        imageLayout.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] imageData = ms.ToArray();
                        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                        response.Content = new ByteArrayContent(imageData);
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                        return response;
                    }
                    else
                    {
                        var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Transaction not found");
                        return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                    }
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "No image selected");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
    }
}
