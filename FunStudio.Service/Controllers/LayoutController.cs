﻿using FS.Common.HttpCustom;
using FunStudio.Service.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FunStudio.Service.Controllers
{
    [RoutePrefix("api/layout")]
    public class LayoutController : ApiController
    {
        /// <summary>
        /// Danh sách layout
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlist")]
        public HttpResponseMessage GetList()
        {
            try
            {
                var data = new ResponseModel();
                if (WebApiConfig.Layouts != null && WebApiConfig.Layouts.Count > 0)
                {
                    data.totalRecord = WebApiConfig.Layouts.Count;
                    data.data = WebApiConfig.Layouts;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Chi tiết 1 layout
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyid")]
        public HttpResponseMessage GetById(long? id)
        {
            try
            {
                var data = WebApiConfig.Layouts.FirstOrDefault(x => x.Id == id);
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Danh sách layout theo frame
        /// </summary>
        /// <param name="frameId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyframeid")]
        public HttpResponseMessage GetByFrameId(long? frameId)
        {
            try
            {
                var data = new ResponseModel();
                var dataLayout = WebApiConfig.Layouts.Where(x => x.FrameId == frameId).ToList();
                if (dataLayout != null && dataLayout.Count > 0)
                {
                    data.totalRecord = dataLayout.Count;
                    data.data = dataLayout;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
    }
}
