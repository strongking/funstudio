﻿using EOSDigital.API;
using EOSDigital.SDK;
using FS.AviFile;
using FS.Common.File;
using FS.Common.HttpCustom;
using FunStudio.Service.Models.Response;
using FunStudio.Service.Session;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static FS.Common.Helper.Enums;

namespace FunStudio.Service.Controllers
{
    [RoutePrefix("api/canon")]
    public class CanonController : ApiController
    {
        static List<StreamingSession> sessions = new List<StreamingSession>();
        static List<Bitmap> listImage = new List<Bitmap>();
        static CanonAPI APIHandler = new CanonAPI();
        static List<Camera> CamList;
        static Camera MainCamera;
        static object ErrLock = new object();
        static object LvLock = new object();
        static object _lock = new object();

        /// <summary>
        /// Danh sách camera
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getlist")]
        public HttpResponseMessage GetList()
        {
            try
            {
                CamList = APIHandler.GetCameraList();
                var data = new ResponseModel();
                if (CamList != null && CamList.Count > 0)
                {
                    data.totalRecord = CamList.Count;
                    data.data = CamList;
                }
                var httpResponseExtensions = new HttpResponseExtensions(HttpStatusCode.OK, "Success", data);
                return Request.CreateResponse(HttpStatusCode.OK, httpResponseExtensions, "application/json");
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
        /// <summary>
        /// Mở phiên làm việc của camera canon
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("opensession")]
        public HttpResponseMessage OpenSessionCamera(long? id)
        {
            try
            {
                if (CamList.Count > 0)
                {
                    id = id != null ? id : CamList.FirstOrDefault().ID;
                    OpenSession(id.Value);
                    var responeResult = new HttpBase(HttpStatusCode.OK, "Success");
                    return Request.CreateResponse(HttpStatusCode.OK, responeResult, "application/json");
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Not found camera");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Liveview của camera ứng với tỷ lệ khung hình
        /// </summary>
        /// <param name="token"></param>
        /// <param name="ratio"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("liveview")]
        public HttpResponseMessage GetVideoCamera(CancellationToken token, Ration? ratio)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.PartialContent);
            resp.Headers.Add("Connection", "Keep-Alive");
            Image insertImage = Image.FromFile(@"E:\trump.png");
            int ratioWidth = 0, ratioHeight = 0;
            switch (ratio)
            {
                case Ration.R1:
                    ratioWidth = 3;
                    ratioHeight = 2;
                    break;
                case Ration.R2:
                    ratioWidth = 2;
                    ratioHeight = 3;
                    break;
                case Ration.R3:
                    ratioWidth = 4;
                    ratioHeight = 3;
                    break;
                case Ration.R4:
                    ratioWidth = 3;
                    ratioHeight = 4;
                    break;
                case Ration.R5:
                    ratioWidth = 5;
                    ratioHeight = 4;
                    break;
                case Ration.R6:
                    ratioWidth = 4;
                    ratioHeight = 5;
                    break;
                default:
                    ratioWidth = 3;
                    ratioHeight = 2;
                    break;
            }
            resp.Content = new PushStreamContent(
               async (outputStream, httpContent, transportContext) =>
               {
                   try
                   {
                       StreamingSession ss = StreamOn(data =>
                       {
                           if (token.IsCancellationRequested)
                           {
                               throw new Exception();
                           }
                           lock (_lock)
                           {

                               String boundaryConstant = "myboundary";
                               var boundary = System.Text.Encoding.Unicode.GetBytes(boundaryConstant);
                               httpContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                               // byte[] imageStream = ReadAllBytes(data);

                               Bitmap imageliveOrigin = new Bitmap(data);
                               int width = imageliveOrigin.Width;
                               int height = imageliveOrigin.Height;
                               if (width * ratioHeight > height * ratioWidth)
                               {
                                   width = height * ratioWidth / ratioHeight;
                               }
                               else
                               {
                                   height = width * ratioHeight / ratioWidth;
                               }

                               // Calculate the coordinates of the top-left corner of the portion of the original image
                               int x = (imageliveOrigin.Width - width) / ratioWidth;
                               int y = (imageliveOrigin.Height - height) / ratioWidth;

                               Bitmap imagelive = new Bitmap(width, height);
                               //Graphics.FromImage(imagelive).DrawImage(imageliveOrigin, 0, 0, width, height);
                               using (Graphics g = Graphics.FromImage(imagelive))
                               {
                                   g.DrawImage(imageliveOrigin, new System.Drawing.Rectangle(0, 0, width, height), new System.Drawing.Rectangle(x, y, width, height), GraphicsUnit.Pixel);
                               }
                               imageliveOrigin.Dispose();

                               //Graphics graphics = Graphics.FromImage(imagelive);
                               //graphics.DrawImage(insertImage, new System.Drawing.Rectangle(imagelive.Width / 3, 50, imagelive.Width / 3, imagelive.Height));
                               listImage.Add(imagelive);
                               byte[] imageStream = ImageToByte(imagelive);
                               resp.Content.Headers.ContentLength = imageStream.Length;
                               var h = this.CreateHeader(imageStream.Length);
                               var f = this.CreateFooter();
                               outputStream.WriteAsync(h, 0, h.Length);
                               outputStream.WriteAsync(imageStream, 0, imageStream.Length);
                               outputStream.WriteAsync(f, 0, f.Length);
                               outputStream?.Flush();
                               outputStream?.Dispose();
                           }
                       });
                       await ss.WaitAsync();
                   }
                   catch
                   {

                   }
                   finally
                   {
                       // Close output stream as we are done
                       outputStream.Close();
                   }
               });
            resp.Content.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/x-mixed-replace; boundary=frame");
            //await resp.Content.ReadAsStreamAsync();
            return resp;
        }

        /// <summary>
        /// Thực hiện chụp ảnh với transaction
        /// </summary>
        /// <param name="transactionid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("takephoto/{transactionid}")]
        public HttpResponseMessage TakePhoto(string transactionid)
        {
            try
            {
                if (listImage != null && listImage.Count() > 0)
                {
                    var image = listImage[listImage.Count() - 1];
                    FileService.SaveFileImage(image, transactionid);
                    byte[] imageData = ImageToByte(image);
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new ByteArrayContent(imageData);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    return response;
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Not found camera");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        /// <summary>
        /// Thực hiện xử lý video với transaction
        /// </summary>
        /// <param name="transactionid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("recordvideo/{transactionid}")]
        public HttpResponseMessage RecordVideo(string transactionid)
        {
            try
            {
                if (listImage != null && listImage.Count() > 0)
                {
                    var bitmapList = new List<Bitmap>();
                    bitmapList.AddRange(listImage);
                    string directory = HttpContext.Current.Server.MapPath("~/App_Data/");
                    var pathCheck = Path.Combine(directory, "FileMedia", transactionid);
                    if (!Directory.Exists(pathCheck)) Directory.CreateDirectory(pathCheck);
                    AviManager aviManager = new AviManager($@"{pathCheck}\{DateTime.Now.ToFileTime().ToString()}.avi", false);
                    VideoStream aviStream = aviManager.AddVideoStream(false, 25, bitmapList[0]);
                    foreach (var bitmap in bitmapList)
                    {
                        aviStream.AddFrame(bitmap);
                    }
                    aviManager.Close();
                    var responeResult = new HttpBase(HttpStatusCode.OK, "Success");
                    return Request.CreateResponse(HttpStatusCode.OK, responeResult, "application/json");
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Not found camera");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        [HttpGet]
        [Route("startrecordvideo")]
        public HttpResponseMessage StartRecordVideo()
        {
            try
            {
                if (CamList.Count > 0 && MainCamera != null)
                {
                    Recording state = (Recording)MainCamera.GetInt32Setting(PropertyID.Record);
                    if (state != Recording.On)
                    {
                        MainCamera.StartFilming(true);
                    }
                    var responeResult = new HttpBase(HttpStatusCode.OK, "Success");
                    return Request.CreateResponse(HttpStatusCode.OK, responeResult, "application/json");
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Not found camera");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }

        [HttpGet]
        [Route("stoprecordvideo")]
        public HttpResponseMessage StopRecordVideo()
        {
            try
            {
                if (CamList.Count > 0 && MainCamera != null)
                {
                    Recording state = (Recording)MainCamera.GetInt32Setting(PropertyID.Record);
                    if (state == Recording.On)
                    {
                        MainCamera.StopFilming(true);
                    }
                    var responeResult = new HttpBase(HttpStatusCode.OK, "Success");
                    return Request.CreateResponse(HttpStatusCode.OK, responeResult, "application/json");
                }
                else
                {
                    var responeResult = new HttpBase(HttpStatusCode.BadRequest, "Not found camera");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
                }
            }
            catch (Exception ex)
            {
                var responeResult = new HttpBase(HttpStatusCode.BadRequest, ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, responeResult, "application/json");
            }
        }
        public StreamingSession StreamOn(Action<Stream> callback)
        {
            StreamingSession session = new StreamingSession(callback);
            sessions.Add(session);
            session.OnSessionEnded += Session_OnSessionEnded;
            return session;
        }

        private void Session_OnSessionEnded(object sender, EventArgs e)
        {
            lock (_lock)
            {
                sessions.Remove(sender as StreamingSession);

                if (!sessions.Any())
                {
                    MainCamera.CloseSession();
                }
            }
        }

        private void MainCamera_LiveViewUpdated(Camera sender, Stream img)
        {
            try
            {
                lock (LvLock)
                {
                    foreach (var session in sessions.ToList())
                    {
                        session.ProvideData(img);
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void MainCamera_DownloadReady(Camera sender, DownloadInfo Info)
        {
            try
            {
                string dir = @"E:\Work\PhotoBooth\env-test\images\";
                sender.DownloadFile(Info, dir);
            }
            catch (Exception ex) { }
        }
        private void OpenSession(long id)
        {
            MainCamera = CamList.FirstOrDefault(x => x.ID == id);
            MainCamera.OpenSession();
            MainCamera.LiveViewUpdated += MainCamera_LiveViewUpdated;
            MainCamera.DownloadReady += MainCamera_DownloadReady;
            if (!MainCamera.IsLiveViewOn)
            {
                MainCamera.StartLiveView();
            }
        }

        public static byte[] ImageToByte(Image img)
        {
            //Thread.Sleep(20);
            //img.Save(@"E:\Work\PhotoBooth\env-test\images\" + DateTime.Now.ToString("yy-mm-dd-mm-hh-ss") + ".png", ImageFormat.Png);
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public byte[] ReadAllBytes(Stream instream)
        {
            if (instream is MemoryStream)
                return ((MemoryStream)instream).ToArray();

            using (var memoryStream = new MemoryStream())
            {
                instream.CopyTo(memoryStream);
                //instream.Close();
                return memoryStream.ToArray();
            }
        }
        private byte[] CreateHeader(int length)
        {
            string header =
                "--frame" + "\r\n" +
                "Content-Type:image/jpeg\r\n" +
                "Content-Length:" + length + "\r\n\r\n";

            return Encoding.ASCII.GetBytes(header);
        }

        private byte[] CreateFooter()
        {
            return Encoding.ASCII.GetBytes("\r\n");
        }
    }
}
