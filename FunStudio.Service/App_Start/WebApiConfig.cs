﻿using FS.Common.Helper;
using FunStudio.Service.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace FunStudio.Service
{
    public static class WebApiConfig
    {
        public static List<FrameEntity> Frames { get; set; }
        public static List<LayoutEntity> Layouts { get; set; }
        public static List<ThemeEntity> Themes { get; set; }
        public static List<ElementEntity> Elements { get; set; }
        public static List<FilterEntity> Filters { get; set; }
        public static void Register(HttpConfiguration config)
        {
            try
            {
                string filePathFrame = HttpContext.Current.Server.MapPath("~/App_Data/XmlData/Frame.xml");
                XmlDocument xdocFrame = new XmlDocument();
                xdocFrame.Load(filePathFrame);
                Frames = (DataHelper.ConvertXmlToList<FrameEntity>(xdocFrame)).ToList();
            }
            catch { }
            try
            {
                string filePathLayout = HttpContext.Current.Server.MapPath("~/App_Data/XmlData/Layout.xml");
                XmlDocument xdocLayout = new XmlDocument();
                xdocLayout.Load(filePathLayout);
                Layouts = (DataHelper.ConvertXmlToList<LayoutEntity>(xdocLayout)).ToList();
            }
            catch { }
            try
            {
                string filePathTheme = HttpContext.Current.Server.MapPath("~/App_Data/XmlData/Theme.xml");
                XmlDocument xdocTheme = new XmlDocument();
                xdocTheme.Load(filePathTheme);
                Themes = (DataHelper.ConvertXmlToList<ThemeEntity>(xdocTheme)).ToList();
            }
            catch { }
            try
            {
                string filePathElement = HttpContext.Current.Server.MapPath("~/App_Data/XmlData/Element.xml");
                XmlDocument xdocElement = new XmlDocument();
                xdocElement.Load(filePathElement);
                Elements = (DataHelper.ConvertXmlToList<ElementEntity>(xdocElement)).ToList();
            }
            catch { }
            try
            {
                string filePathFilter = HttpContext.Current.Server.MapPath("~/App_Data/XmlData/Filter.xml");
                XmlDocument xdocFilter = new XmlDocument();
                xdocFilter.Load(filePathFilter);
                Filters = (DataHelper.ConvertXmlToList<FilterEntity>(xdocFilter)).ToList();
            }
            catch { }
            // Web API configuration and services
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
