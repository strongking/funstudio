﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FunStudio.Service.Session
{
    public class StreamingSession
    {
        public StreamingSession(Action<Stream> Callback)
        {
            this.Callback = Callback;
        }

        private Action<Stream> Callback;
        private TaskCompletionSource<dynamic> Completion = new TaskCompletionSource<dynamic>();

        public event EventHandler OnSessionEnded;

        public Task WaitAsync(int? timeout = null)
        {
            if (timeout.HasValue)
            {
                return Task.WhenAny(Task.Delay(timeout.Value), this.Completion.Task);
            }

            return this.Completion.Task;
        }

        public void ProvideData(Stream data)
        {
            try
            {
                this.Callback(data);
            }
            catch (Exception)
            {
                this.EndSession();
            }
        }

        public void EndSession()
        {
            this.Completion.SetResult(null);
            if (this.OnSessionEnded != null)
            {
                this.OnSessionEnded(this, null);
            }
        }
    }
}