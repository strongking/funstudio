﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FS.Common.HttpCustom
{
    public class HttpBase
    {
        public HttpStatusCode code { get; set; }
        public object message { get; set; }

        public HttpBase(HttpStatusCode code, object message)
        {
            this.code = code;
            this.message = message;
        }
    }

}
