﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FS.Common.File
{
    public class FileService
    {
        public static String SaveFileImage(Bitmap file, string transaction)
        {
            try
            {
                var fileName = DateTime.Now.ToFileTime().ToString() + ".png";
                string directory = HttpContext.Current.Server.MapPath("~/App_Data/");
                var pathCheck = Path.Combine(directory, "FileMedia", transaction);
                if (!Directory.Exists(pathCheck)) Directory.CreateDirectory(pathCheck);
                file.Save(pathCheck + "/" + fileName);
                return transaction + "/" + fileName;
            }
            catch
            {
                return null;
            }
        }
    }
}
