﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Common.Helper
{
    public static class Enums
    {
        public enum Ration
        {
            /// <summary>
            /// 3:2
            /// </summary>
            R1 = 1,
            /// <summary>
            /// 2:3
            /// </summary>
            R2 = 2,
            /// <summary>
            /// 4:3
            /// </summary>
            R3 = 3,
            /// <summary>
            /// 3:4
            /// </summary>
            R4 = 4,
            /// <summary>
            /// 5:4
            /// </summary>
            R5 = 5,
            /// <summary>
            /// 4:5
            /// </summary>
            R6 = 6,
        }
    }
}
