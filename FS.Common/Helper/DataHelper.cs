﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace FS.Common.Helper
{
    public class DataHelper
    {
        public static IList<T> ConvertXmlToList<T>(XmlDocument xdoc)
        {
            List<T> reval = new List<T>();
            try
            {
                string json = JsonConvert.SerializeXmlNode(xdoc);
                var nds = xdoc.GetElementsByTagName(typeof(T).Name);
                foreach (var item in nds)
                {
                    var jsNode = JsonConvert.SerializeObject(item);
                    var resultDynamic = JsonConvert.DeserializeObject<dynamic>(jsNode);
                    if (resultDynamic != null)
                    {
                        var listDynamic = resultDynamic[typeof(T).Name];
                        var jsonData = JsonConvert.SerializeObject(listDynamic);
                        var data = JsonConvert.DeserializeObject<T>(jsonData);
                        reval.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return reval;
        }
    }
}
